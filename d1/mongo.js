//We are going to create 4 documents to use for the discussion

db.fruits.insertMany([
            {
                name : "Apple",
                color : "Red",
                stock : 20,
                price: 40,
                supplier_id : 1,
                onSale : true,
                origin: [ "Philippines", "US" ]
            },

            {
                name : "Banana",
                color : "Yellow",
                stock : 15,
                price: 20,
                supplier_id : 2,
                onSale : true,
                origin: [ "Philippines", "Ecuador" ]
            },

            {
                name : "Kiwi",
                color : "Green",
                stock : 25,
                price: 50,
                supplier_id : 1,
                onSale : true,
                origin: [ "US", "China" ]
            },

            {
                name : "Mango",
                color : "Yellow",
                stock : 10,
                price: 120,
                supplier_id : 2,
                onSale : false,
                origin: [ "Philippines", "India" ]
            }
        ]);


//MongoDB Agggregation

	// An aggregate is a cluster of things that have come or have been brought together

	//In MongoDb, aggregatetion operations group values from multiple documents together

db.fruits.aggregate([

		{$match: {onSale: true}},
		{$group: {_id: "$supplier_id", total:{$sum: "$stock"} } }
	]);

	//Aggregation Pilpeline Stages
		//Aggregation is typically done in 2-3 steps.  Each process in aggregation is called a stage.

		/*
			$match - is used to match or get doceents which satisfies the condition

			//syntax: {$match: {field: ,value>}}


			$group - allows us to group documents and creat analysis out of the group documents

			_id: in the gourp stage, essentially associates an id to our results

			_id: also, it determines the number of groups

		*/

		//Field projection

		db.fruits.aggregate([
			{$match: {onSale: true}},
			{$group: {_id: "$supplier_id", total:{$sum: "$stock"} } },
			{$project: {_id:0} }
		]);


		/*
			the $project can be used when aggragating data to include / exclude fields fomr the returned results

			Syntax: {field: 0/1}

			1 shows up
			
			0 does not show up

		*/

		//Sort aggregated results

		db.fruits.aggregate([

		{$match: {onSale: true}},
		{$group: {_id: "$supplier_id", total:{$sum: "$stock"} } },
		{$sort: {total: -1}}
		]);


		/*
			$sort can be used to changed the order of aggregate results

			Syntax:
			{$sort{field: 1/-1}}

			1 	- ascending
			-1	- descending

		*/

		//aggrgating result based on array field


		db.fruits.aggregate([
			{$unwind: "$origin"}
		]);

		/* 	
			$unwind deconstructs an array field froma a field with our array cvalue to output a result for each element

			Syntax

			{$unwind": field}
		*/


	//Mini Activity No.1: find how many fruits are yellow
		//$count
			//syntax
				//{$count: <string>}



		db.fruits.aggregate([
			{$unwind: "$origin"},
			{$group: {_id: "$origin", kinds: {$sum:1}}}
		]);

	//Mini Activity No.2: find how many fruit stocks that are less than or equal to 10


		db.fruits.aggregate([
			{$match: {stock: {$lte:10}}},
			{$count: "Fruits less than 10"}

		]);


	//Mini Activity No.3: find fruits for each group of supplier that has the maximum stocks

		db.fruits.aggregate([

			{$match: {onSale: true}},
			{$group: {_id: "$supplier_id", stock:{$max: "$maxstock"} } }
		]);

		


