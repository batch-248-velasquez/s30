//A: Use the count operator to count the total number of fruits with stock more than 20.

db.fruits.aggregate([
    { $match: { stock: { $gte: 20 } } },
    { $count: "Fruits more than 20" }
]);




//B: Use the average operator to get the average price of fruits onSale per supplier.

db.fruits.aggregate(
    [
    { $match: { onSale: true } },
    { 
        $group:
         { 
            _id: "$supplier_id",
            avgPrice: {$avg: "&price" }}}
]);



//C: Use the max operator to get the highest price of a fruit per supplier.


db.fruits.aggregate([
    { $match: { onSale: true } },
    {$group: {_id: "$supplier_id", price:{$max: "$price"} } }
]);


//D. Use the min operator to get the lowest price of a fruit per supplier.

db.fruits.aggregate([

    {$group: {_id: "$supplier_id", price:{$min: "$price"} } }
]);